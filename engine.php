
<?php
    if(isset($_REQUEST["get"])) {
        $file = fopen("./database.json", "r") or exit("Erreur d'ouverture du fichier de base de donnée");
        $data = "";
        while($line = fgets($file)) {
            $data .= $line;
        }
        echo $data;
        fclose($file);
        exit();
    }

    elseif(isset($_REQUEST["update"])) {
        $file = fopen("./database.json", "w");
        fwrite($file, $_REQUEST["update"]);
        fclose($file);
        echo "Fichier de base de données mis à jour avec succès :)";
        exit();
    }

    else {
        echo "Nothing to do here ;)";
    }
?>