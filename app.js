$(document).ready( ()=> {
	console.log("Hello");
    let client_name = $("#client_name");
    let food_code = $("#food_code");
    let quantity = $("#quantity");
    let add_order_button = $("#add_order_button");
    let search = $("input[type=search]");
    let limit_entry = $("#limit");
    let limit = limit_entry.val();
    let regex = "";
    let search_data = "";
    let position = 0;
    let taxe = 180;


    /*
        Ajouter autant d'article que l'on veut dans le catalogue, juste en suivant de pattern objet

        "code_article" : {
            "name" : "nom de l'aticle",
            "price": prix de l'article
        }
    */
    let foods_catalog = {
        "at1" : {
            "name" : "Chawarma",
            "price" : 1000
        },

        "at2" : {
            "name" : "Hamburger",
            "price" : 1300
        },

        "at3" : {
            "name" : "Pizza",
            "price" : 3500
        },

        "at4" : {
            "name" : "Sandwich",
            "price" : 1100
        },

        "at5" : {
            "name" : "Tackos",
            "price" : 1700
        },

        "at6" : {
            "name" : "Croissant",
            "price" : 350
        },

        "at7" : {
            "name" : "Canette Fanta",
            "price" : 500
        },

        "at8" : {
            "name" : "Bierre",
            "price" : 700
        }
    }

    // insertion des articles dans le code html
    for(let i = 0; i < Object.keys(foods_catalog).length; i++) {
        let key = Object.keys(foods_catalog)[i];
        food_code.append($(`<option value="${key}">${foods_catalog[key].name}</option>`));
    }

    let orders = {};
    
    let load = ()=>{
        $.post("./engine.php", {get:''}, (data) => {
            try{

                orders = JSON.parse(data);
            }
            catch(exception) {
                orders = {orders : []};
            }
            display_row(orders.orders);
            notify("Récupération des données en cours ...", "primary");
        });
    }

    load();

    $("#reload").click(load);

    add_order_button.click( (event) => {

        let client = client_name.val();
        let food = foods_catalog[food_code.val()];
        
        let quantityBought = quantity.val();
        let id = 0;
        if(client_name.val().trim().length > 0 && quantity.val() > 0 && food_code.val() != "null") {
            // notify("Commande prise en charge. En cours de traitement.", "info");
            $("#reset_form").trigger("click");

            try{
                id = orders.orders[orders.orders.length-1].id+1
            } catch(exception) {
                id = 0;
            }

            let order = 
            {
                "id": id,
                "clientName" : `${client}`,
                "food" : food,
                "quantityBought" : `${quantityBought}`
            }

            $.post("./engine.php", {get:''}, (data) => {
                try{

                    orders = JSON.parse(data);
                }
                catch(exception) {
                    orders = {orders : []};
                }

                orders.orders.push(order);

                $.post("./engine.php", {update: JSON.stringify(orders)}, (data) => {
                    notify(data, "success");
                    display_row(orders.orders);
                });
            });
                    

            
        } else {
            notify("Veillez remplir tous les champs de formulaire puis réessayez.", "danger");
        }
    } );

    search.keyup((event) => {
        search_data = $(event.currentTarget).val();
        position = 0;
        display_row();
    });

    limit_entry.change(()=>{
        position = 0;
        limit = limit_entry.val();
        display_row(orders.orders);
    })

    let display_row = (array = orders.orders) => {

        // recherche de donnéé
        regex = RegExp(`${search_data}`, 'i');
        if(search_data.length > 0) {
            array = orders.orders.filter(order => {
                if(
                    RegExp(regex).test(order.clientName) || RegExp(regex).test(order.food.name)) {
                    return order;
                }
            });
        }


        table_1 = $("#orders_history"); // table des historiques
        table_2 = $("#clients_orders"); // table des utilisateurs
        table_1.empty();
        table_2.empty();
        if(array.length == 0) {
            table_1.html("Aucun enregistrement");
            table_2.html("Aucun enregistrement");
            return;
        }
        let order = Object();

        // table 1
        let thead = $(`
            <tr class="text-primary">
                <th>N°</th>
                <th>Client</th>
                <th>Article</th>
                <th>Quantité</th>
                <th>Prix TTC</th>
                <th>Action</th>
            </tr>`
        );
        table_1.append(thead);

        // table 2
        thead = $(`
            <tr class="text-info">
                <th>N°</th>
                <th>Client</th>
                <th>Action</th>
            </tr>`
        );
        table_2.append(thead);

        let limit_max = parseInt(position)+parseInt(limit) < array.length ? parseInt(position)+parseInt(limit) : array.length;
        for(let index=position; index < limit_max ; index++){
            order = array[index];
            let row = $(`
                <tr style='transition: all 0.1s; display: none'>
                    <td>#${order.id}</td>
                    <td>${order.clientName.toString().toUpperCase().replace(regex, `<span class='text-primary'>${search_data.toUpperCase()}</span>`)}</td>
                    <td>${order.food.name.toUpperCase().replace(regex, `<span class='text-primary'>${search_data.toUpperCase()}</span>`)}</td>
                    <td class="">${order.quantityBought}</td>
                    <td class="text-primary">${order.food.price * order.quantityBought+taxe} XOF</td>
                    <td class="text-danger">
                        <a href='#${order.id}' class='delete_order'><i class="material-icons text-danger" data-id="${index}">delete</i></a>
                    </td>
                </tr>
            `);
            table_1.append(row);
            row.fadeIn(1000);

            row = $(`
                <tr style='transition: all 0.1s; display: none'>
                    <td>#${order.id}</td>
                    <td>${order.clientName.toString().toUpperCase().replace(regex, `<span class='text-info'>${search_data.toUpperCase()}</span>`)}</td>
                    <td class="text-justify">
                        <a href="#${order.id}" class="print_order"><i class="material-icons text-success">print</i></a>
                    </td>
                </tr>
            `);
            table_2.append(row);
            row.fadeIn(1000);

        };

        let pagination_bloc = "";
        for(let i=0; i < Math.ceil(array.length/limit); i++)
        {
            let active = i == Math.ceil(position/limit) ? "active" : "";
            pagination_bloc += `
                <li class="page-item ${active}" style="transition: all 0.5s">
                    <a href="#${i}" class="page-link page_number">${i+1}</a>
                </li>`;
        }
        $(".pagination").html(pagination_bloc);
    }

    $("body").on("click", ".page_number", (event)=>{
        let target = $(event.currentTarget);
        let page = parseInt(target.attr("href").replace("#", ""));
        position = parseInt(page*limit);
        display_row(orders.orders);
    });

    // deletion
    $("body").on("click", ".delete_order", (event)=>{
        let target = $(event.currentTarget);
        let id = target.attr("href").replace("#", "");

        orders.orders.forEach((order, index)=>{
            if(order.id == id) {

                notify("Suppression de la commande N° "+id, "warning");
                orders.orders.splice(index, 1); // suppression élément de liste

                // actualisation de l'objet
                $.post("./engine.php", {update: JSON.stringify(orders)}, (data) => {
                    notify(data, "success");
                    target.parents("tr").hide(200);
                    display_row();
                });

                return;
            }
        });
    });

    // print
    let currentOrder = {};
    $("body").on("click", ".print_order", (event)=>{
        let target = $(event.currentTarget);
        let id = target.attr("href").replace("#", "");
        notify("Imprimer Commande Pour: "+$(target.parent().siblings()[1]).text(), "warning");

        orders.orders.filter((order) => {
            if(order.id == id) {
                currentOrder = order;

                let order_card = $(`
                    <div class="container" style="width: 500px">
                        <div class="card card-nav-ts">
                            <div class="card-header card-header-rose">
                                <div class="nav-tabs-navigation text-center">
                                    <div class="nav-tabs-wrapper">
                                        <div>
                                            <h4>MINI RESTO MANAGMENT APP</h4>
                                            <strong>
                                                Téléphone: (+221) 78 457 37 63 / (+221) 76 220 56 10
                                                <br/>
                                                Adresse: Dakar, Liberte 6 Ext
												<br/>
												Caissier: Christian Barnabé CHABI
                                            </strong>
                                            <hr/>

                                            <h3 class="text-white">
                                                <i class="material-icons"></i> NUMÉRO DE COMMANDE : #${currentOrder.id}
                                            </h3>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card-body">
                                <table style="width:100% ! important">
                                    <tr>
                                        <th>Client: </th>
                                        <td>${currentOrder.clientName}</td>
                                    </tr>
                                    <tr>
                                        <th>Article: </th>
                                        <td>${currentOrder.food.name}</td>
                                    </tr>
                                    <tr>
                                        <th>Quantité: </th>
                                        <td>${currentOrder.quantityBought}</td>
                                    </tr>
                                </table>

                                <hr>

                                <table style="width:100% ! important">
                                    <tr>
                                        <th>Prix Hors Taxe: </th>
                                        <td>${currentOrder.quantityBought * currentOrder.food.price} XOF</td>
                                    </tr>
                                    <tr>
                                        <th>TVA: </th>
                                        <td>${taxe} XOF</td>
                                    </tr>
                                    <tr>
                                        <th>Net à Payer: </th>
                                        <td>${currentOrder.quantityBought * currentOrder.food.price + taxe} XOF</td>
                                    </tr>
                                </table>
                                <hr>
                            </div>
                            
                            <div class="card-footer text-right bg-light d-inline">
                                ${Date()}
                            </div>
                        </div>
                    </div>
                `);

                let print_window = window.open('', 'Imprimer', 'height=500, width=500');
                    print_window.document.write(`
                        <html class="" style="padding:30px; height: 300px">
                            <head>
                                <link rel="stylesheet" href="./lib/css/gfont.css">
                                <link rel="stylesheet" href="./lib/css/material-kit.min.css">
                            </head>

                            <body class="">
                                ${order_card.html()}
                            </body>
                        </html>
                    `)
                print_window.document.close();
                setTimeout(()=>{
                    print_window.print();
                }, 2000);
                return;
            }
        });
    })
    

    let notify = (message, alert_type="info") => {
        let html = $(`
        <div class='alert alert-${alert_type} animate' style='border-radius:5px; opacity:0; transition: all 1s; position: relative; top: -50px; margin-bottom: 10px'>
            <div class='container'>
                <div class='alert-icon'>
                    <i class='material-icons'>info_outline</i>
                </div>
                <button type='button' class='close' data-dismiss='alert' aria-label='Close'>
                    <span aria-hidden='true'><i class='material-icons'>clear</i></span>
                </button>
                ${message}
            </div>
        </div>`);

        html.animate( {
            "top":"10px",
            "opacity":"1"
        }, 50)
        $("#notifications").prepend(html);

        setTimeout(()=>{
            html.remove();
        }, 5000);
    }
})
